const $Winston = require('winston');

require('winston-mongodb');

const mongoServiceProvider = require('../db/mongo-service');

const Log = new $Winston.Logger();

const level = process.env.LOG_LEVEL || (process.env.NODE_ENV === 'development' && 'debug') || 'info';
const colorize = process.env.NODE_ENV === 'producation' ? false : true;

Log.configure({
  transports: [
    new ($Winston.transports.Console)({
      colorize,
      level,
      handleExceptions: true,
      humanReadableUnhandledException: true,
      prettyPrint: true,
      timestamp: true
    }),
    new ($Winston.transports.MongoDB)({
      db: new Promise((resolve) => {
        // waiting connection
        mongoServiceProvider.log.native((() => {
          resolve(mongoServiceProvider.log.connection.db);
        }))
      }),
      level
    })
  ]
});

module.exports = Log;
