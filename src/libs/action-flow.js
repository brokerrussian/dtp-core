const $config = require('config');

const actionFlow = require('action-flow')({
  db: $config.get('db.mongo-service.db'),
  host: $config.get('mongo.host'),
  port: $config.get('mongo.port')
});

module.exports = actionFlow;