const config = require('config');
const connectionConfig = config.get('db.mongo-pub-bot');
const jMongo = require('just-mongo');

const mongo = new jMongo(connectionConfig);

module.exports = {
  postDrafts: mongo.collection('postDrafts'),
  userScript: mongo.collection('userScript')
};