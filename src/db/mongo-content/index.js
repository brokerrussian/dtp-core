const config = require('config');
const connectionConfig = config.get('db.mongo-content');
const jMongo = require('just-mongo');

const mongo = new jMongo(connectionConfig);

module.exports = {
  contentBlacklist: mongo.collection('contentBlacklist'),
  contentCache: mongo.collection('contentCache'),
  contentIndex: mongo.collection('contentIndex'),
  postCovers: mongo.collection('postCovers'),
  posts: mongo.collection('posts'),
};