const config = require('config');
const connectionConfig = config.get('db.mongo-service');
const jMongo = require('just-mongo');

const mongo = new jMongo(connectionConfig);

module.exports = {
  queue: mongo.collection('__af__queue'),
  log: mongo.collection('log')
};