const config = require('config');
const connectionConfig = config.get('db.mongo-front');
const jMongo = require('just-mongo');

const mongo = new jMongo(connectionConfig);

module.exports = {
  sessions: mongo.collection('sessions')
};