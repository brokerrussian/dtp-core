const env = process.env.NODE_ENV;
let file;

if (env === 'producation') {
  module.exports = require('./producation.js');
} else {
  module.exports = require('./development.js');
}