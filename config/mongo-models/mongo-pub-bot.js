const models = {};
const Schema = require('../schema');

models['userScript'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      cmd: Schema.anyString,
      payload: Schema.anyObject
    },
    required: ['user_id', 'cmd']
  }
};

models['postDrafts'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      account_id: Schema.uuid,
      path: Schema.path,
      tags: Schema.tags,
      language: Schema.language,
      createdDate: Schema.anyNumber,
      controlKey: Schema.uuid,
      done: {
        type: 'boolean'
      }
    },
    required: ['user_id', 'path', 'done', 'createdDate']
  }
};

module.exports = models;